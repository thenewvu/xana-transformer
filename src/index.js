'use strict';

const _     = require('lodash');
const async = require('neo-async');

/**
 * Process a schema.
 * @param {Object} schema
 * @returns {Array}
 * @throws {TypeError} If the scheme is invalid.
 * @private
 */
function processSchema(schema) {
  return _.keys(schema).map((path) => {
    const func = schema[path];
    if (!_.isFunction(func)) {
      throw new TypeError(
        `Expects a standard transform but got "${func}"`
      );
    }
    return {
      path,
      func
    };
  });
}

/**
 * Transform an object at a given path and a transform function.
 * @param obj
 * @param path
 * @param func
 * @param done
 */
function transformObj(obj, path, func, done) {
  const value = _.get(obj, path);
  if (value === undefined) {
    return done(null);
  }
  _.set(obj, path, func(value));
  done(null);
}

/**
 * Create a transformer.
 * @param {Object} scheme
 */
function create(scheme, opts) {
  const transformers = processSchema(scheme);
  return (obj, done) => {
    const eachTransformer = (transformer, next) => {
      transformObj(obj, transformer.path, transformer.func, next);
    };
    async.each(transformers, eachTransformer, done);
  };
}

module.exports = {
  create
};
