'use strict';

const path    = require('path');
const fs      = require('fs');
const chai    = require('chai');
const yaml    = require('js-yaml');
const decache = require('decache');
const expect  = chai.expect;

const testDir       = path.dirname(__filename);
const testDataFiles = fs.readdirSync(testDir)
                        .filter((file) => file.endsWith('.yaml'))
                        .map((file) => path.join(testDir, file));

testDataFiles.forEach((file) => {
  const testCases = yaml.load(fs.readFileSync(file));
  Object.keys(testCases).forEach((describeName) => {
    describe(describeName, function () {
      Object.keys(testCases[describeName]).forEach((itName) => {
        it(itName, function (done) {
          const itData = testCases[describeName][itName];
          decache('../src/index.js');
          const transfomer = require('../src/index.js');
          transfomer.create(itData.schema)(itData.obj, (err) => {
            expect(err).to.deep.equal(itData.err);
            expect(itData.obj).to.deep.equal(itData.res);
            done();
          });
        });
      });
    });
  });
});
